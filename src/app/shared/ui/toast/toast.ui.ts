import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { ToastOptions } from '@ionic/core';

@Injectable({
    providedIn: 'root'
})
export class ToastUI {

    constructor(private toastCtrl: ToastController) { }

    public async primary(message: string, position = 'bottom', duration = 3000) {
        await this.presentToast(message, 'primary', position, duration);
    }

    public async secondary(message: string, position = 'bottom', duration = 3000) {
        await this.presentToast(message, 'secondary', position, duration);
    }

    public async light(message: string, position = 'bottom', duration = 3000) {
        await this.presentToast(message, 'light', position, duration);
    }

    public async white(message: string, position = 'bottom', duration = 3000) {
        await this.presentToast(message, 'white', position, duration);
    }

    public async success(message: string, position = 'bottom', duration = 3000) {
        await this.presentToast(message, 'success', position, duration);
    }

    public async danger(message: string, position = 'bottom', duration = 3000) {
        await this.presentToast(message, 'danger', position, duration);
    }

    private async presentToast(message: string, color = 'primary', position = 'bottom', duration = 3000) {
        const toast = await this.toastCtrl.create({
            message: message,
            position: position == 'top' ? 'top' : position == 'middle' ? 'middle' : 'bottom',
            color: color,
            duration: duration,
            mode: 'ios',
            buttons: [
                {
                    icon: 'close',
                    handler: () => { }
                }
            ]
        });
        toast.present();
    }

    public async toastWithOptions(options: ToastOptions) {
        const toast = await this.toastCtrl.create({
            header: options.header,
            message: options.message,
            color: (options.color ? options.color : 'primary'),
            duration: (options.duration ? options.duration : 3000),
            position: (options.position ? options.position : 'bottom'),
            cssClass: options.cssClass,
            mode: options.mode ? options.mode : 'ios',
            buttons: options.buttons ? options.buttons : [
                {
                    icon: 'close',
                    handler: () => { }
                }
            ]
        });
        toast.present();
    }
}
