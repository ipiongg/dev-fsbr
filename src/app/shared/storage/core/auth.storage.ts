import { User } from '../../../models/user.model';
import { CryptographyUtil } from '../../utils/cryptography/cryptography.util';
import * as jwt_decode from 'jwt-decode';
import { GenericResultCommand } from 'src/app/commands/core/generic-result.command';

export class AuthStorage {

    private static userStorageKey = 'dev-fsbr.user';
    private static tokenStorageKey = 'dev-fsbr.token';

    public static setUser(user: User) {
        localStorage.setItem(this.userStorageKey, CryptographyUtil.encrypt(user));
    }

    public static getUser(): User {
        const data = localStorage.getItem(this.userStorageKey);
        const decrypted = CryptographyUtil.decrypt(data);

        return decrypted ? decrypted : null;
    }

    public static setToken(token: string) {
        localStorage.setItem(this.tokenStorageKey, CryptographyUtil.encrypt(token));
    }

    public static getToken(): string {
        const data = localStorage.getItem(this.tokenStorageKey);
        const decrypted = CryptographyUtil.decrypt(data);

        return decrypted ? decrypted : null;
    }

    public static isTokenValid(): GenericResultCommand {
        const decodedToken = AuthStorage.getDecodedToken();
        if (!decodedToken || !decodedToken.exp)
            return new GenericResultCommand(true);

        const expiresIn = decodedToken.exp;
        if ((Math.floor((new Date).getTime() / 1000)) >= expiresIn)
            return new GenericResultCommand(true, 'Sua sessão expirou. Faça login novamente');
        else
            return new GenericResultCommand(false);
    }

    private static getDecodedToken(): any {
        const token = AuthStorage.getToken();
        if (!token)
            return null;

        try {
            return jwt_decode(token);
        } catch (Error) {
            return null;
        }
    }

    public static clear() {
        localStorage.clear();
    }
}