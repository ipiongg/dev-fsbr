import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { async } from '@angular/core/testing';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { GenericResultCommand } from 'src/app/commands/core/generic-result.command';
import { User } from 'src/app/models/user.model';
import { AuthenticatedService } from 'src/app/services/authenticated.service';
import { EMAIL_PATTERN } from 'src/app/shared/constants/patterns.constant';
import { AuthStorage } from 'src/app/shared/storage/core/auth.storage';
import { ToastUI } from 'src/app/shared/ui/toast/toast.ui';
import { PeopleManagementPage } from '../people-management/people-management.page';

@Component({
  selector: 'app-authenticated',
  templateUrl: './authenticated.page.html',
  styleUrls: ['./authenticated.page.scss'],
})
export class AuthenticatedPage implements OnInit {

  public form: FormGroup;
  public validationMessages: any = {};


  constructor(
    private navCtrl: NavController,
    private authService: AuthenticatedService,
    private formBuilder: FormBuilder,
    private toastUI: ToastUI,
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.email,
        Validators.pattern(EMAIL_PATTERN)
      ])],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20)
      ])]
    })
    this.validationMessages = {
      email: [
        { type: "required", message: "O campo e-mail é obrigatório." },
        { type: "pattern", message: "Este e-mail não é válido." },
      ],
      password: [
        { type: "required", message: "O campo Senha é obrigatório." },
        {
          type: "minlength",
          message: "Este campo deve ter pelo menos 8 caracteres.",
        },
        {
          type: "maxlength",
          message: "Este campo deve ter no máximo 20 caracteres.",
        },
      ],

    };
  }

  public submit() {
    if (this.form.invalid) {
      this.toastUI.danger('Dados inválidos');
      return;
    }

    this.auth();
  }


  /**
   * Função utilizada para realizar autenticação e guardar o token
   */
  private async auth() {
    this.authService.auth(this.form.value).then(async (response: GenericResultCommand) => {
      let user: User = new User();
      user = response.data.user
      AuthStorage.setUser(user);
      AuthStorage.setToken(response.data.token);
      this.navCtrl.navigateRoot('/');
    }, (error: HttpErrorResponse) => {
      if (error.status == 401) {
        this.toastUI.danger('Email ou senha invalidos, tente novamente.')
      }
      else if (error.status == 404) {
        this.toastUI.danger('Erro no servidor, tente novamente mais tarde')
      }
      // console.log('error', error)

    })
  }


  register() {
    this.navCtrl.navigateForward('/people-management')
  }

}
