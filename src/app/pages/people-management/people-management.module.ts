import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PeopleManagementPageRoutingModule } from './people-management-routing.module';

import { PeopleManagementPage } from './people-management.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    PeopleManagementPageRoutingModule
  ],
  declarations: [PeopleManagementPage]
})
export class PeopleManagementPageModule {}
