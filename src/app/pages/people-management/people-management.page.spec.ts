import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PeopleManagementPage } from './people-management.page';

describe('PeopleManagementPage', () => {
  let component: PeopleManagementPage;
  let fixture: ComponentFixture<PeopleManagementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeopleManagementPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PeopleManagementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
