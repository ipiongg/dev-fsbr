import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { async } from '@angular/core/testing';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GenericResultCommand } from 'src/app/commands/core/generic-result.command';
import { PeopleManagementService } from 'src/app/services/people-management.service';
import { EMAIL_PATTERN } from 'src/app/shared/constants/patterns.constant';
import { ToastUI } from 'src/app/shared/ui/toast/toast.ui';
import { filter } from 'rxjs/operators';


@Component({
  selector: 'app-people-management',
  templateUrl: './people-management.page.html',
  styleUrls: ['./people-management.page.scss'],
})
export class PeopleManagementPage implements OnInit {
  public form: FormGroup;
  public validationMessages: any = {};

  //  (CPF, Nome, E-mail, etc).

  registers: any = []

  constructor(
    private toastUI: ToastUI,
    private formBuilder: FormBuilder,
    private service: PeopleManagementService

  ) { }

  ngOnInit() {
    this.buildForm()


    /**Ao confirmar essa função é executada e faz a atualização da lista. */
    this.service._refreshNeeded.subscribe(() => {

    });
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      CPF: ['', Validators.compose([
        Validators.required,
      ])],
      nome: ['', Validators.compose([
        Validators.required,

      ])],
      email: ['', Validators.compose([
        Validators.required,
        Validators.email,
        Validators.pattern(EMAIL_PATTERN)
      ])]
    })
    this.validationMessages = {
      email: [
        { type: "required", message: "O campo e-mail é obrigatório." },
        { type: "pattern", message: "Este e-mail não é válido." },
      ],
      nome: [
        { type: "required", message: "O campo Nome é obrigatório." },
      ],

      CPF: [
        { type: "required", message: "O campo CPF é obrigatório." },
      ],

    };
  }

  public register() {

    if (this.form.valid) {
      this.registers.push(this.form.value);
      this.toastUI.success('Cadastro realizado com sucesso!')
      this.form.reset()
    }

    /**
    A função a seguir é utilizada caso tenha a api.
     */
    this.service.create(this.form.value).then(async (response: GenericResultCommand) => {
      if (response.data) {
        this.toastUI.success('Cadastro realizado com sucesso!')
      }
    }, (error: HttpErrorResponse) => {
      if (error.status == 500) {
        this.toastUI.danger('Error no servidor, tente novamente mais tarde')
      }
    })

  }


  public update(id, data) {
   
    /**
    A função a seguir é utilizada caso tenha a api.
     */
    this.service.update(id, data).then(async (response: GenericResultCommand) => {
      if (response.data) {
        this.toastUI.success('Cadastro alterado com sucesso!')
      }
    }, (error: HttpErrorResponse) => {
      if (error.status == 500) {
        this.toastUI.danger('Error no servidor, tente novamente mais tarde')
      }
    })
    // console.log(this.form.value)
  }



  public delete(id) {
    const registros = this.registers
    console.log('meus registros', registros)
    var index = registros.indexOf(id);
    registros.splice(index, 1)
    // console.log('registros', registros)
    /**
    A função a seguir é utilizada caso tenha a api.
     */
    this.service.delete(id).then(async (response: GenericResultCommand) => {
      if (response.data) {
        this.toastUI.success('Cadastro removido com sucesso!')
      }
    }, (error: HttpErrorResponse) => {
      if (error.status == 500) {
        this.toastUI.danger('Error no servidor, tente novamente mais tarde')
      }
    })
    // console.log(this.form.value)
  }

}
