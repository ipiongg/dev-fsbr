import { Injectable } from "@angular/core";
import {
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
} from "@angular/common/http";
import { AuthStorage } from '../shared/storage/core/auth.storage';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler) {
        if (!req.headers.has("Content-Type")) {
            req = req.clone({
                headers: req.headers.set("Content-Type", "application/json"),
            });
        }

        if (!req.headers.has("Accept")) {
            req = req.clone({
                headers: req.headers.set("Accept", "application/json"),
            });
        }

        // req = req.clone({ headers: req.headers.set('Access-Control-Allow-Origin', '*') });

        const token = AuthStorage.getToken();
        if (token) {
            req = req.clone({
                setHeaders: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + token,
                },
            });
        }

        return next.handle(req);
    }
}
