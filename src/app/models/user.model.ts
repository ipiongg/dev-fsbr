import { Entity } from './core/entity.model';

export class User extends Entity {
    constructor(
        public email?: string,
        public password?: string,

    ) {
        super();
    }
}