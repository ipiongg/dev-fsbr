import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: "",
    redirectTo: 'authenticated',
    pathMatch: 'full'
  },
  {
    path: "people-management",
    loadChildren: () => import('./pages/people-management/people-management.module').then(m => m.PeopleManagementPageModule),
    // canActivate: [AuthGuard],
  },
  
  {
    path: '',
    loadChildren: () => import('./pages/authenticated/authenticated.module').then(m => m.AuthenticatedPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
