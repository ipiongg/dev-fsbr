import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { GenericResultCommand } from '../commands/core/generic-result.command';
import { AuthStorage } from '../shared/storage/core/auth.storage';
import { ToastUI } from '../shared/ui/toast/toast.ui';

@Injectable({
    providedIn: "root",
})
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private toastUI: ToastUI) { }

    canActivate() {
        const token = AuthStorage.getToken();
        // console.log("meu token", token)

        if (!token) {
            this.router.navigateByUrl('/authenticated');
            return false;
        }

        const result: GenericResultCommand = AuthStorage.isTokenValid();

        if (result.error) {
            if (result.message)
                this.toastUI.primary(result.message);

            this.router.navigateByUrl('/authenticated');
            return false;
        }

        return true;
    }
}
