import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class PeopleManagementService {

  constructor(private http: HttpClient) { }

  private _refreshNeeded$ = new Subject<void>();

  get _refreshNeeded() {
    return this._refreshNeeded$;
  }


  create(data): Promise<any> {
    return this.http
      .post(`${environment.apiUrl}create/people`, data)
      .pipe(
        tap(() => {
          this._refreshNeeded$.next();
        })
      )
      .toPromise();
  }

  update(id, data): Promise<any> {
    return this.http
      .put(`${environment.apiUrl}create/people${id}`, data)
      .pipe(
        tap(() => {
          this._refreshNeeded$.next();
        })
      )
      .toPromise();
  }

  delete(id): Promise<any> {
    return this.http
      .delete(`${environment.apiUrl}create/people${id}`)
      .pipe(
        tap(() => {
          this._refreshNeeded$.next();
        })
      )
      .toPromise();
  }


}
