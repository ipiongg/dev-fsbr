import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { tap } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { GenericResultCommand } from '../commands/core/generic-result.command';
@Injectable({
  providedIn: 'root'
})
export class AuthenticatedService {

  constructor(private http: HttpClient) { }

  auth(data): Promise<GenericResultCommand> {
    return this.http.post<GenericResultCommand>(`${environment.apiUrl}auth/signin`, data).toPromise();
  }
}
