import { TestBed } from '@angular/core/testing';

import { PeopleManagementService } from './people-management.service';

describe('PeopleManagementService', () => {
  let service: PeopleManagementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PeopleManagementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
